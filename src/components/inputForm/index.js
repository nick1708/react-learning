import React, {useState} from "react"

export default function InputForm (props) {
    const [title, setTitle] = useState("");
    const [completed, setCompleted] = useState(false);

    const handleTitleChange = ({target : {value}}) => {
        setTitle(value);
    }

    const handleCompletedChange = ({target : {checked}}) => {
        setCompleted(checked);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        props.saveTodo({
            id: props.arraySize + 1,
            title,
            completed
        });
    }

    return (
        <form onSubmit={handleSubmit}>
            <div className="form-row">
                <div className="col-5">
                    <input onChange={handleTitleChange} value={title} type="text" className="form-control" name="title" placeholder="Title" />
                </div>
                <div className="col-5">
                    <div className="custom-control custom-checkbox">
                        <input checked={completed} onChange={handleCompletedChange} type="checkbox" className="custom-control-input" id="completed" name="completed" />
                        <label style={{color: "black"}} className="custom-control-label" htmlFor="completed">Completed</label>
                    </div>
                </div>
                <div className="col-2">
                    <button type="submit" className="btn btn-primary w-100">Añadir</button>                    
                </div>
            </div>
        </form>
    );
}
