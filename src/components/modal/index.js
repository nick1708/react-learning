import React, {Fragment, useState, useEffect} from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';

export default function ModalUpdate(props) {    
    const [title, setTitle] = useState(props.record.title);
    const [completed, setCompleted] = useState(props.record.completed);

    useEffect(() => {
        setTitle(props.record.title);
        setCompleted(props.record.completed);

        return () => {
            // Se ejecuta al desmontar el componente
        }
    }, [props])

    const handleTitleChange = ({target : {value}}) => {
        setTitle(value);
    }

    const handleCompletedChange = ({target : {checked}}) => {
        setCompleted(checked);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        props.editTodo({
            id: props.record.id,
            title, completed
        });
    }

    return (
        <Fragment>
            <Modal isOpen={props.isOpen} toggle={props.toggleModal}>
                <ModalHeader toggle={props.toggleModal}>Edit ToDo</ModalHeader>
                <ModalBody>
                    <form onSubmit={handleSubmit}>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Title</label>
                            <input onChange={handleTitleChange} type="text" value={title} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                        </div>
                        <div className="form-group form-check">
                            <input onChange={handleCompletedChange} type="checkbox" className="form-check-input" checked={completed} id="exampleCheck1" />
                            <label className="form-check-label" htmlFor="exampleCheck1">completed</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Guardar</button>
                    </form>
                </ModalBody>
            </Modal>
        </Fragment>
    );
}