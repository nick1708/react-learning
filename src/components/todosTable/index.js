import React, { PureComponent, Fragment } from "react";
import axios from "axios";
import InputForm from "../inputForm";
import Modal from "../modal";

class TodosTable extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            to_dos: [],
            isOpen: false,
            todo: {
                id: 0,
                title: "",
                completed: false
            }
        }
    }
    
    componentDidMount() {
        this.fetchAPI();
    }

    fetchAPI = async () => {
        const {data} = await axios.get("https://jsonplaceholder.typicode.com/todos");
        const firsts = data.slice(0, 4);

        this.setState({ to_dos: firsts });
    }

    setTodo = (todo) => {
        this.setState({
            to_dos: [...this.state.to_dos, todo]
        });
    }    

    listTodos = () => {
        const procesado = this.state.to_dos.map((item, index) => {
            return(
                <tr key={index}>
                    <th scope="row">{item.id}</th>
                    <td>{item.title}</td>
                    <td>
                        { item.completed ? <span className="badge badge-success">Completado</span> : <span className="badge badge-danger">Pendiente</span> }
                    </td>
                    <td>
                        <button onClick={this.deleteTodo(item.id)} className="btn btn-danger">Delete</button>
                    </td>
                    <td>
                        <button className="btn btn-warning" onClick={this.setRecordToEdit(item)}>Update</button>
                    </td>
                </tr>
            ) 
        });

        return procesado;
    }

    deleteTodo = (id) => () => {
        const newList = this.state.to_dos.filter((item) => {
            return item.id !== id;
        });

        this.setState({ to_dos: newList });
    }

    setRecordToEdit = (record) => () => {
        this.setState({ todo: record }, this.toggleModal);
    }

    toggleModal = () => {
        this.setState((prevState) => {
            return { isOpen: !prevState.isOpen }
        });
    }

    updateTodo = (todo) => {
        const { to_dos } = this.state;

        const found = to_dos.find((item) => {
            return todo.id === item.id;
        });
        
        const index = to_dos.indexOf(found);

        let newState = [...to_dos];
        newState[index] = {...todo};

        this.setState({ to_dos: newState });
    }

    render() {
        return (
            <Fragment>
                <div className="card">
                    <div className="card-body">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Completed</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              {this.listTodos()}
                            </tbody>
                        </table>
                    </div>
                    <div className="card-footer">
                        <InputForm arraySize={this.state.to_dos.length} saveTodo={this.setTodo} />
                    </div>                
                </div>
                <Modal isOpen={this.state.isOpen} record={this.state.todo} editTodo={this.updateTodo} toggleModal={this.toggleModal} />
            </Fragment>
        )
    }
}

export default TodosTable;