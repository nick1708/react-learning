import React from 'react';
import TodosTable from './components/todosTable';
import './App.css';

function App() {
  return (
    <div className="App">
      <div className="App-header">
        <div className="container">
          <TodosTable />
        </div>
      </div>
    </div>
  );
}

export default App;
